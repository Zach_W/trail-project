import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { FiEye, FiEyeOff } from "react-icons/fi";
import "./LoginPage.css";

const LoginPage = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [passwordShown, setPasswordShown] = useState(false);
    const { login } = useToken();
    const { token } = useAuthContext();
    const [errorMessage, setErrorMessage] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        if (token) {
            navigate("/home");
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);

    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        await login(username, password);
        if (!token) {
            setErrorMessage("Invalid username or password");
        }
    };

    return (
        <div>
            <h2 className="text-top">Resume Your Journey</h2>
            {errorMessage && (
                <p className="text-danger error">{errorMessage}</p>
            )}
            <form id="loginForm" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label>User Name</label>
                    <input
                        type="string"
                        value={username}
                        onChange={handleUsernameChange}
                        className="form-control input-field"
                    />
                </div>
                <div className="form-group password">
                    <label>
                        Password
                        <i
                            onClick={togglePasswordVisiblity}
                            className="password-icon ms-2">
                            {passwordShown ? <FiEyeOff /> : <FiEye />}
                        </i>
                    </label>
                    <input
                        type={passwordShown ? "text" : "password"}
                        value={password}
                        onChange={handlePasswordChange}
                        className="form-control input-field"
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-outline-secondary mt-1">
                    Sign in
                </button>
                <div className="mt-4">
                    <p className="text-center">
                        New to ArtOasis?
                        <Link to="/signup" className="ms-2">
                            <button className="btn btn-outline-secondary btn-sm">
                                Join now
                            </button>
                        </Link>
                    </p>
                </div>
            </form>
        </div>
    );
};

export default LoginPage;
