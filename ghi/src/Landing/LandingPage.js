import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import "./LandingPage.css";

// Discover, Equip, and Explore the Great Outdoors

const LandingPage = () => {
    const { token } = useToken();
    const navigate = useNavigate();

    useEffect(() => {
        if (token !== null) {
            navigate("/home");
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);
    return (
        <div>
            <h2>Discover, Equip, and Explore the Great Outdoors</h2>
            <div className="container mt-5">
                <div className="row">
                    <div className="col d-flex justify-content-center align-items-center">
                        <Link to="/signup">
                            <button className="btn btn-outline-secondary">
                                Embark
                            </button>
                        </Link>
                    </div>
                    <div className="col d-flex justify-content-center align-items-center">
                        <Link to="/login">
                            <button className="btn btn-outline-secondary">
                                Resume Your Journey
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LandingPage;
