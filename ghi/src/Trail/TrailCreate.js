import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./TrailCreate.css";

const TrailCreate = () => {
    const navigate = useNavigate();
    const [loadouts, setLoadouts] = useState([]);
    const [name, setName] = useState();
    const [image_url, setImage_url] = useState();
    const [location, setLocation] = useState();
    const [length, setLength] = useState();
    const [lowest_elevation, setLowestElevation] = useState();
    const [highest_elevation, setHighestElevation] = useState();
    const [elevation_gain, setElevation_Gain] = useState();
    const [days, setDays] = useState();
    const [nights, setNights] = useState();
    const [start_date, setStart_Date] = useState();
    const [end_date, setEnd_date] = useState();
    const [loadout_id, setloadout_id] = useState();

    useEffect(() => {
        const getLoadouts = async () => {
            const url = `${process.env.REACT_APP_API_HOST}/api/loadouts/currentUser/`;
            const response = await fetch(url, { credentials: "include" });
            if (response.ok) {
                const data = await response.json();
                setLoadouts(data);
            }
        };
        getLoadouts();
    }, []);

    const handleNameChange = (event) => {
        setName(event.target.value);
    };
    const handleImageURLChange = (event) => {
        setImage_url(event.target.value);
    };

    const handleLocationChange = (event) => {
        setLocation(event.target.value);
    };

    const handleLengthChange = (event) => {
        setLength(event.target.value);
    };

    const handleLowestElevationChange = (event) => {
        setLowestElevation(event.target.value);
    };

    const handleHighestElevationChange = (event) => {
        setHighestElevation(event.target.value);
    };

    const handleElevationGainChange = (event) => {
        setElevation_Gain(event.target.value);
    };

    const handleDaysChange = (event) => {
        setDays(event.target.value);
    };

    const handleNightsChange = (event) => {
        setNights(event.target.value);
    };

    const handleStartDateChange = (event) => {
        setStart_Date(event.target.value);
    };

    const handleEndDateChange = (event) => {
        setEnd_date(event.target.value);
    };

    const handleLoadoutIdChange = (event) => {
        setloadout_id(event.target.value);
    };

    const handleTrailSubmit = async (event) => {
        event.preventDefault();
        const url = `${process.env.REACT_APP_API_HOST}/api/trails`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                name,
                image_url,
                location,
                length,
                lowest_elevation,
                highest_elevation,
                elevation_gain,
                days,
                nights,
                start_date,
                end_date,
                loadout_id,
            }),
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            const newTrail = await response.json();
            setName("");
            setImage_url("");
            setLocation("");
            setLength("");
            setLowestElevation("");
            setHighestElevation("");
            setElevation_Gain("");
            setDays("");
            setNights("");
            setStart_Date("");
            setEnd_date("");
            setloadout_id("");
            navigate(`/trail/${newTrail.id}`);
        }
    };
    return (
        <div className="text-center">
            <h1> Craft a Trail!</h1>
            {loadouts ? (
                <form onSubmit={handleTrailSubmit} id="trail-create">
                    <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <div className="mb-3">
                                <label htmlFor="name" className="form-label">
                                    Name
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={name}
                                    onChange={handleNameChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor="image_url"
                                    className="form-label">
                                    Image URL
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="image_url"
                                    value={image_url}
                                    onChange={handleImageURLChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor="location"
                                    className="form-label">
                                    Location
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="location"
                                    value={location}
                                    onChange={handleLocationChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="length" className="form-label">
                                    Length
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="length"
                                    value={length}
                                    onChange={handleLengthChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor="lowest_elevation"
                                    className="form-label">
                                    Lowest Elevation
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="lowest_elevation"
                                    value={lowest_elevation}
                                    onChange={handleLowestElevationChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor="highest_elevation"
                                    className="form-label">
                                    Highest Elevation
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="highest_elevation"
                                    value={highest_elevation}
                                    onChange={handleHighestElevationChange}
                                />
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-12">
                            <div className="mb-3">
                                <label
                                    htmlFor="elevation_gain"
                                    className="form-label">
                                    Elevation Gain
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="elevation_gain"
                                    value={elevation_gain}
                                    onChange={handleElevationGainChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="days" className="form-label">
                                    Days
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="days"
                                    value={days}
                                    onChange={handleDaysChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="nights" className="form-label">
                                    Nights
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="nights"
                                    value={nights}
                                    onChange={handleNightsChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor="start_date"
                                    className="form-label">
                                    Start Date
                                </label>
                                <input
                                    type="date"
                                    className="form-control"
                                    id="start_date"
                                    value={start_date}
                                    onChange={handleStartDateChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor="end_date"
                                    className="form-label">
                                    End Date
                                </label>
                                <input
                                    type="date"
                                    className="form-control"
                                    id="end_date"
                                    value={end_date}
                                    onChange={handleEndDateChange}
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor="loadout_id"
                                    className="form-label">
                                    Loadout ID
                                </label>
                                {loadouts === null ? (
                                    <p>Please Create a Loadout First!</p>
                                ) : (
                                    <select
                                        type="text"
                                        className="form-select"
                                        id="loadout_id"
                                        value={loadout_id}
                                        onChange={handleLoadoutIdChange}>
                                        <option value="">
                                            Select a Loadout
                                        </option>
                                        {loadouts.map((loadout) => (
                                            <option
                                                key={loadout.id}
                                                value={loadout.id}>
                                                {loadout.name}
                                            </option>
                                        ))}
                                    </select>
                                )}
                            </div>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-outline-secondary">
                        Submit
                    </button>
                </form>
            ) : (
                <p> Please Craft a Loadout before Venturing on!</p>
            )}
        </div>
    );
};
export default TrailCreate;
