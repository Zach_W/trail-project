import React from "react";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./TrailDetails.css";

const TrailDetails = () => {
    const { trail_id } = useParams();
    const [trail, setTrail] = useState(null);
    const [loadout, setLoadout] = useState(null);
    const [user, setUser] = useState(null);
    const [gears, setGears] = useState(null);
    const [categories, setCategories] = useState(null);

    useEffect(() => {
        const getTrail = async () => {
            const url = `${process.env.REACT_APP_API_HOST}/api/trails/${trail_id}`;
            const response = await fetch(url, { credentials: "include" });
            if (response.ok) {
                const data = await response.json();
                setTrail(data.trail);
                setLoadout(data.loadout);
                setUser(data.user);
                setGears(data.gear);
                setCategories(data.categories);
            }
        };
        getTrail();
    }, [trail_id]);

    const renderUserModal = () => (
        <div
            className="modal fade"
            id="userModal"
            tabIndex="-1"
            aria-labelledby="userModalLabel"
            aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="userModalLabel">
                            User Profile
                        </h5>
                        <button
                            type="button"
                            className="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                            <div className="col-md-4">
                                <img
                                    src={user.profile_picture}
                                    alt={user.username}
                                    className="img-fluid modal-profile-picture"
                                />
                            </div>
                            <div className="col-md-8 ps-4 pt-3">
                                <p>
                                    <strong>Email:</strong> {user.email}
                                </p>
                                <p>
                                    <strong>Username:</strong> {user.username}
                                </p>
                                <p>
                                    <strong>First Name:</strong>{" "}
                                    {user.first_name}
                                </p>
                                <p>
                                    <strong>Last Name:</strong> {user.last_name}
                                </p>
                                <p>
                                    <strong>About:</strong> {user.about}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button
                            type="button"
                            className="btn btn-secondary"
                            data-bs-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );

    const renderLoadoutModal = () => (
        <div
            className="modal fade"
            id="loadoutModal"
            tabIndex="-1"
            aria-labelledby="loadoutModalLabel"
            aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="loadoutModalLabel">
                            {loadout.name} Loadout
                        </h5>
                        <button
                            type="button"
                            className="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <div className="d-flex justify-content-between align-items-center">
                            <p>
                                <strong>Season:</strong> {loadout.season}
                            </p>
                            <p>
                                <strong>Total Weight:</strong>{" "}
                                {loadout.total_weight}
                            </p>
                        </div>
                        <table className="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Category</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                {categories.map((category) => (
                                    <React.Fragment key={category.id}>
                                        <tr>
                                            <td colSpan="3">
                                                <strong>{category.name}</strong>
                                            </td>
                                        </tr>
                                        {gears
                                            .filter(
                                                (gear) =>
                                                    gear.category_id ===
                                                    category.id
                                            )
                                            .map((gear) => (
                                                <tr key={gear.id}>
                                                    <td></td>
                                                    <td>{gear.name}</td>
                                                    <td>{gear.quantity}</td>
                                                </tr>
                                            ))}
                                    </React.Fragment>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <div className="modal-footer">
                        <button
                            type="button"
                            className="btn btn-secondary"
                            data-bs-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
    return (
        <div className="d-flex justify-content-center align-items-center">
            {trail ? (
                <div className="card trail-card">
                    {renderUserModal()}
                    {gears && renderLoadoutModal()}
                    <div className="card-header d-flex align-items-center">
                        <img
                            className="card-profile-picture"
                            src={user.profile_picture}
                            alt={user.username}
                        />
                        <h3 className="card-title ms-3">{user.username}</h3>
                        <div className="ms-auto">
                            <button
                                type="button"
                                className="btn btn-secondary btn-sm"
                                data-bs-toggle="modal"
                                data-bs-target="#userModal">
                                View User Profile
                            </button>
                        </div>
                    </div>
                    <img
                        className="trail-img"
                        src={trail.image_url}
                        alt="trail"
                    />
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-6">
                                <table className="table table-hover">
                                    <tbody>
                                        <tr>
                                            <th
                                                className="table-header"
                                                colSpan="2">
                                                {trail.name} Trail
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Length:</strong>
                                            </td>
                                            <td>{trail.length}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>
                                                    Highest Elevation:
                                                </strong>
                                            </td>
                                            <td>{trail.highest_elevation}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>
                                                    Lowest Elevation:
                                                </strong>
                                            </td>
                                            <td>{trail.lowest_elevation}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Elevation Gain:</strong>
                                            </td>
                                            <td>{trail.elevation_gain}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Days:</strong>
                                            </td>
                                            <td>{trail.days}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Nights:</strong>
                                            </td>
                                            <td>{trail.nights}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Start Date:</strong>
                                            </td>
                                            <td>{trail.start_date}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>End Date:</strong>
                                            </td>
                                            <td>{trail.end_date}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            {loadout && (
                                <div className="col-md-6">
                                    <table className="table table-hover">
                                        <tbody>
                                            <tr>
                                                <th
                                                    className="table-header"
                                                    colSpan="2">
                                                    Loadout
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Name:</strong>
                                                </td>
                                                <td>{loadout.name}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Season:</strong>
                                                </td>
                                                <td>{loadout.season}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Total Weight:
                                                    </strong>
                                                </td>
                                                <td>{loadout.total_weight}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    {gears && (
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-sm"
                                            data-bs-toggle="modal"
                                            data-bs-target="#loadoutModal">
                                            View Loadout
                                        </button>
                                    )}
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            ) : (
                <p>Loading...</p>
            )}
        </div>
    );
};
export default TrailDetails;
