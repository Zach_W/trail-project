import { useEffect, useState } from "react";
import { useNavigate, NavLink } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import "./Nav.css";

function Nav() {
    const navigate = useNavigate();
    const { logout, token } = useToken();
    const [loggedIn, setLoggedIn] = useState(false);

    const handleLogout = () => {
        logout();
        navigate("/");
    };

    useEffect(() => {
        const checkToken = async () => {
            if (token) {
                setLoggedIn(true);
            } else {
                setLoggedIn(false);
            }
        };
        checkToken();
    }, [token, loggedIn]);

    return (
        <div className="d-flex justify-content-center align-items-center">
            <nav
                className="navbar navbar-expand-lg bg-body-tertiary"
                aria-label="Eleventh navbar example">
                <div className="container-fluid nav-width">
                    <NavLink className="navbar-brand" to="/">
                        Trail Bound Odyssey
                    </NavLink>
                    <button
                        className="navbar-toggler "
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbar-collapse-container"
                        aria-controls="navbar-collapse-container"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon navbar-dark"></span>
                    </button>
                    <div
                        className="collapse navbar-collapse"
                        id="navbar-collapse-container">
                        <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/home">
                                    Home
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    to="/trail/create">
                                    Trail
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link"
                                    to="/loadout/create">
                                    Loadout
                                </NavLink>
                            </li>
                            <li className="nav-item dropdown">
                                <NavLink
                                    className="nav-link dropdown-toggle"
                                    href="#"
                                    data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    Profile
                                </NavLink>
                                <ul
                                    id="profiledropdown"
                                    className="dropdown-menu">
                                    {loggedIn === true && (
                                        <>
                                            <li className="text-center">
                                                <NavLink
                                                    className="nav-link"
                                                    aria-current="page"
                                                    to="profile">
                                                    My Profile
                                                </NavLink>
                                            </li>
                                            <li className="d-grid gap-2">
                                                <button
                                                    onClick={handleLogout}
                                                    className="btn btn-link">
                                                    Logout
                                                </button>
                                            </li>
                                        </>
                                    )}

                                    {loggedIn === false && (
                                        <>
                                            <li>
                                                <NavLink
                                                    className="nav-link"
                                                    aria-current="page"
                                                    to="/login">
                                                    Login
                                                </NavLink>
                                            </li>
                                            <li>
                                                <NavLink
                                                    className="nav-link"
                                                    aria-current="page"
                                                    to="/signup">
                                                    SignUp
                                                </NavLink>
                                            </li>
                                        </>
                                    )}
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    );
}

export default Nav;
