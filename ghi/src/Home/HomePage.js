import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import "./HomePage.css";

const HomePage = () => {
    const [trails, setTrails] = useState([]);
    const { token } = useAuthContext();
    const navigate = useNavigate();
    useEffect(() => {
        if (!token) {
            navigate("/");
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);

    useEffect(() => {
        const getTrails = async () => {
            const url = `${process.env.REACT_APP_API_HOST}/api/trails/`;
            const response = await fetch(url, { credentials: "include" });
            if (response.ok) {
                const data = await response.json();
                setTrails(data);
            }
        };
        getTrails();
    }, []);
    return (
        <div>
            {trails.map((trail_with_loadout) => {
                const trail = trail_with_loadout.trail;
                const loadout = trail_with_loadout.loadout;
                return (
                    <div key={trail.id}>
                        <div className="d-flex justify-content-center align-items-center">
                            <div className="card trail-card">
                                <img
                                    className="card-img-top trail-img"
                                    src={trail.image_url}
                                    alt="trail"
                                />
                                <div className="card-body">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group list-group-flush trail-title">
                                            {trail.name} | {trail.location}
                                        </li>
                                        <li className="list-group-item">
                                            Length: {trail.length}
                                        </li>
                                        <li className="list-group-item">
                                            Highest Elevation:{" "}
                                            {trail.highest_elevation}
                                        </li>
                                        <li className="list-group-item">
                                            Loadout: {loadout.name}
                                        </li>
                                        <Link
                                            to={`/trail/${trail.id}`}
                                            className="list-group-item list-group-item-action">
                                            <button className="btn btn-secondary mt-2r">
                                                Details
                                            </button>
                                        </Link>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
};

export default HomePage;
