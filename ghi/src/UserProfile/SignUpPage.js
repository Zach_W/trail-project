import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { FiEye, FiEyeOff } from "react-icons/fi";
import "./SignUpPage.css";

const SignUpForm = () => {
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [first_name, setFirst_name] = useState("");
    const [last_name, setLast_name] = useState("");
    const [profile_picture, setProfilePicture] = useState("");
    const [about, setAbout] = useState("");
    const [passwordShown, setPasswordShown] = useState(false);
    const { register, token } = useToken();
    const navigate = useNavigate();
    const [errorMessage, setErrorMessage] = useState("");

    if (token) {
        navigate("/home");
    }

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };
    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };
    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };
    const handleConfirmPasswordChange = (event) => {
        setConfirmPassword(event.target.value);
    };
    const handleFirstNameChange = (event) => {
        setFirst_name(event.target.value);
    };
    const handleLastNameChange = (event) => {
        setLast_name(event.target.value);
    };
    const handleProfilePictureChange = (event) => {
        setProfilePicture(event.target.value);
    };
    const handleAboutChange = (event) => {
        setAbout(event.target.value);
    };
    const togglePasswordVisibility = () => {
        setPasswordShown(!passwordShown);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        try {
            if (
                !email ||
                !username ||
                !password ||
                !confirmPassword ||
                !first_name ||
                !last_name ||
                !profile_picture ||
                !about
            ) {
                setErrorMessage("Please enter all the required information.");
                return;
            } else if (password === confirmPassword) {
                const userUrl = `${process.env.REACT_APP_API_HOST}/api/users`;
                const userData = {
                    email: email,
                    username: username,
                    password: password,
                    first_name: first_name,
                    last_name: last_name,
                    profile_picture: profile_picture,
                    about: about,
                };
                register(userData, userUrl);
                setEmail("");
                setUsername("");
                setPassword("");
                setConfirmPassword("");
                setFirst_name("");
                setLast_name("");
                setProfilePicture("");
                setAbout("");
                event.target.reset();
                navigate("/home");
            } else {
                throw new Error("Passwords do not match!");
                //TODO: Maybe put an alert here?
            }
        } catch (error) {
            console.log("Error submitting form:", error);
        }
    };
    return (
        <div className=" mb-3p fixed-inputt">
            <h2 className="text-top">
                Sign Up Now to Start Sharing Your Experiences!
            </h2>
            {errorMessage && (
                <p className="text-danger error">{errorMessage}</p>
            )}
            <form id="SignupForm" onSubmit={handleSubmit}>
                <div className="row">
                    <div className="col-md-6 col-sm-12">
                        <div className=" mb-3">
                            <label>Email:</label>
                            <input
                                type="text"
                                value={email}
                                onChange={handleEmailChange}
                                className="form-control"
                            />
                        </div>
                        <div className=" mb-3">
                            <label>Username:</label>
                            <input
                                type="text"
                                value={username}
                                onChange={handleUsernameChange}
                                className="form-control"
                            />
                        </div>
                        <div className=" mb-3">
                            <label>
                                Password:
                                <i
                                    onClick={togglePasswordVisibility}
                                    className="password-icon ms-2">
                                    {passwordShown ? <FiEye /> : <FiEyeOff />}
                                </i>
                            </label>
                            <input
                                type={passwordShown ? "text" : "password"}
                                value={password}
                                onChange={handlePasswordChange}
                                className="form-control "
                            />
                        </div>
                        <div className=" mb-3">
                            <label>
                                Confirm password:
                                <i
                                    onClick={togglePasswordVisibility}
                                    className="password-icon ms-2">
                                    {passwordShown ? <FiEye /> : <FiEyeOff />}
                                </i>
                            </label>
                            <input
                                type={passwordShown ? "text" : "password"}
                                value={confirmPassword}
                                onChange={handleConfirmPasswordChange}
                                className="form-control "
                            />
                        </div>
                    </div>

                    <div className="col-md-6 col-sm-12">
                        <div className=" mb-3">
                            <label>First Name:</label>
                            <input
                                type="text"
                                value={first_name}
                                onChange={handleFirstNameChange}
                                className="form-control "
                            />
                        </div>
                        <div className=" mb-3">
                            <label>Last Name:</label>
                            <input
                                type="text"
                                value={last_name}
                                onChange={handleLastNameChange}
                                className="form-control "
                            />
                        </div>
                        <div className=" mb-3">
                            <label>Profile Picture URL:</label>
                            <input
                                type="text"
                                value={profile_picture}
                                onChange={handleProfilePictureChange}
                                className="form-control "
                            />
                        </div>
                        <div className=" mb-3">
                            <label>About Me:</label>
                            <textarea
                                rows="5"
                                value={about}
                                onChange={handleAboutChange}
                                className="form-control "
                            />
                        </div>
                    </div>
                </div>
                <button
                    type="submit"
                    className="btn btn-outline-secondary agree">
                    Join Now!
                </button>
            </form>
        </div>
    );
};

export default SignUpForm;
