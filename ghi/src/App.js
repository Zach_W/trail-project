import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthProvider, useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import React from "react";
import Nav from "./Nav";
import "./App.css";
import LandingPage from "./Landing/LandingPage";
import SignUpPage from "./UserProfile/SignUpPage";
import LoginPage from "./Login/LoginPage";
import HomePage from "./Home/HomePage";
import TrailDetails from "./Trail/TrailDetails";
import TrailCreate from "./Trail/TrailCreate";
import LoadoutCreate from "./Loadout/LoadoutCreate";
import LoadoutEdit from "./Loadout/LoadoutEdit";

function App() {
    const { setToken } = useAuthContext();
    setToken("");
    return (
        <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
            <BrowserRouter>
                <Nav />
                <div className="routes">
                    <div className="max-width">
                        <Routes>
                            <Route path="/" element={<LandingPage />} />
                            <Route path="/home" element={<HomePage />} />
                            <Route path="/signup" element={<SignUpPage />} />
                            <Route path="/login" element={<LoginPage />} />
                            <Route
                                path="/trail/:trail_id"
                                element={<TrailDetails />}
                            />
                            <Route
                                path="/trail/create"
                                element={<TrailCreate />}
                            />
                            <Route
                                path="/loadout/:loadout_id"
                                element={<LoadoutEdit />}
                            />
                            <Route
                                path="/loadout/create"
                                element={<LoadoutCreate />}
                            />
                        </Routes>
                    </div>
                </div>
            </BrowserRouter>
        </AuthProvider>
    );
}

export default App;
