import React from "react";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./LoadoutEdit.css";

const LoadoutEdit = () => {
    const { loadout_id } = useParams();
    const [loadout, setLoadout] = useState(null);
    const [categories, setCategories] = useState(null);
    const [gears, setGears] = useState(null);
    const [gearName, setGearName] = useState();
    const [quantity, setQuantity] = useState();
    const [categoryName, setCategoryName] = useState();
    const [reload, setReload] = useState();

    const handleGearSubmit = async (event) => {
        event.preventDefault();
        const gearData = {
            name: gearName,
            quantity: quantity,
        };
        const url = `${process.env.REACT_APP_API_HOST}/api/gears`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(gearData),
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setGearName("");
            setQuantity("");
            setReload();
        }
    };
    const handleCategorySubmit = async (event) => {
        event.preventDefault();
        const categoryData = {
            name: categoryName,
            loadout_id: loadout_id,
        };
        const url = `${process.env.REACT_APP_API_HOST}/api/categories`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(categoryData),
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setCategoryName("");
            setReload();
        }
    };

    useEffect(() => {
        const getLoadout = async () => {
            const url = `${process.env.REACT_APP_API_HOST}/api/loadouts/${loadout_id}`;
            const response = await fetch(url, { credentials: "include" });
            if (response.ok) {
                const data = await response.json();
                setLoadout(data);
            }
        };
        const getCategories = async () => {
            const url = `${process.env.REACT_APP_API_HOST}/api/categories/loadout/${loadout_id}`;
            const response = await fetch(url, { credentials: "include" });
            if (response.ok) {
                const data = await response.json();
                setCategories(data);
            }
        };
        const getGears = async () => {
            const url = `${process.env.REACT_APP_API_HOST}/api/gears/loadout/${loadout_id}`;
            const response = await fetch(url, { credentials: "include" });
            if (response.ok) {
                const data = await response.json();
                setGears(data);
            }
        };
        getLoadout();
        getCategories();
        getGears();
    }, [loadout_id, reload]);

    console.log("gears", gears);
    return (
        <>
            {loadout && (
                <div id="loadout" className="card">
                    <div className="card-header">
                        <h3 className="card-title ms-3">
                            {loadout.name} Loadout
                        </h3>
                    </div>
                    <div className="card-body">
                        <div className="d-flex justify-content-between align-items-center">
                            <p>
                                <strong>Season:</strong> {loadout.season}
                            </p>
                            <p>
                                <strong>Total Weight:</strong>{" "}
                                {loadout.total_weight}
                            </p>
                        </div>
                        <table className="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Category</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                {categories &&
                                    categories.map((category) => (
                                        <React.Fragment key={category.id}>
                                            <tr>
                                                <td colSpan="3">
                                                    <strong>
                                                        {category.name}
                                                    </strong>
                                                </td>
                                            </tr>
                                            {gears &&
                                                gears
                                                    .filter(
                                                        (gear) =>
                                                            gear.category_id ===
                                                            category.id
                                                    )
                                                    .map((gear) => (
                                                        <tr key={gear.id}>
                                                            <td></td>
                                                            <td>{gear.name}</td>
                                                            <td>
                                                                {gear.quantity}
                                                            </td>
                                                        </tr>
                                                    ))}
                                        </React.Fragment>
                                    ))}
                            </tbody>
                        </table>
                        <div className="card-footer">
                            <div className="d-flex justify-content-between">
                                <button
                                    className="btn btn-secondary mt-2"
                                    type="button"
                                    data-bs-toggle="collapse"
                                    data-bs-target="#collapseCategory"
                                    aria-expanded="false"
                                    aria-controls="collapseCategory">
                                    Add Category
                                </button>
                                <button
                                    className="btn btn-secondary mt-2"
                                    type="button"
                                    data-bs-toggle="collapse"
                                    data-bs-target="#collapseGear"
                                    aria-expanded="false"
                                    aria-controls="collapseGear">
                                    Add Gear
                                </button>
                            </div>
                            <div className="collapse" id="collapseCategory">
                                <form onSubmit={handleCategorySubmit}>
                                    <div className="mb-3 mt-3">
                                        <label className="form-label">
                                            Name:
                                        </label>
                                        <input
                                            className="form-control"
                                            type="text"
                                            value={categoryName}
                                            onChange={(e) =>
                                                setCategoryName(e.target.value)
                                            }
                                        />
                                    </div>
                                    <button
                                        className="btn btn-secondary"
                                        type="submit">
                                        Add Category
                                    </button>
                                </form>
                            </div>
                            <div className="collapse" id="collapseGear">
                                <form onSubmit={handleGearSubmit}>
                                    <div className="mb-3 mt-3">
                                        <label className="form-label">
                                            Name:
                                        </label>
                                        <input
                                            className="form-control"
                                            type="text"
                                            value={gearName}
                                            onChange={(e) =>
                                                setGearName(e.target.value)
                                            }
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <label className="form-label">
                                            Quantity:
                                        </label>
                                        <input
                                            className="form-control"
                                            type="number"
                                            value={quantity}
                                            onChange={(e) =>
                                                setQuantity(e.target.value)
                                            }
                                        />
                                    </div>
                                    <button
                                        className="btn btn-secondary"
                                        type="submit">
                                        Add Gear
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};

export default LoadoutEdit;
