import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./LoadoutCreate.css";

const LoadoutCreate = () => {
    const navigate = useNavigate();
    const [name, setName] = useState();
    const [season, setSeason] = useState();
    const [total_weight, setTotal_Weight] = useState();
    const handleNameChange = (event) => {
        setName(event.target.value);
    };
    const handleSeasonChange = (event) => {
        setSeason(event.target.value);
    };
    const handleTotalWeightChange = (event) => {
        setTotal_Weight(event.target.value);
    };
    const handleLoadoutSubmit = async (event) => {
        event.preventDefault();
        const url = `${process.env.REACT_APP_API_HOST}/api/loadouts`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                name,
                season,
                total_weight,
            }),
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            const newLoadout = await response.json();
            setName("");
            setSeason("");
            setTotal_Weight("");
            navigate(`/loadout/${newLoadout.id}`);
        }
    };
    return (
        <div>
            <h1>Build a Loadout!</h1>
            <form onSubmit={handleLoadoutSubmit} id="loadout-create">
                <div className="mb-3">
                    <label htmlFor="name" className="form-label">
                        Name
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="name"
                        value={name}
                        onChange={handleNameChange}
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="season" className="form-label">
                        Season
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="season"
                        value={season}
                        onChange={handleSeasonChange}
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="total_weight" className="form-label">
                        Total Weight
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="total_weight"
                        value={total_weight}
                        onChange={handleTotalWeightChange}
                    />
                </div>
                <button type="submit" className="btn btn-outline-secondary">
                    Submit
                </button>
            </form>
        </div>
    );
};

export default LoadoutCreate;
