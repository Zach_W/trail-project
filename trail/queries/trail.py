from typing import List, Union
from pydantic import BaseModel
from datetime import date, datetime
from queries.pool import pool
from fastapi import HTTPException, status
from decimal import Decimal


class Error(BaseModel):
    message: str


class TrailIn(BaseModel):
    name: str
    image_url: str
    location: str
    length: Decimal
    lowest_elevation: int
    highest_elevation: int
    elevation_gain: int
    days: int
    nights: int
    start_date: date
    end_date: date
    loadout_id: int


class TrailOut(BaseModel):
    id: int
    user_id: int
    name: str
    image_url: str
    location: str
    length: Decimal
    lowest_elevation: int
    highest_elevation: int
    elevation_gain: int
    days: int
    nights: int
    start_date: str
    end_date: str
    loadout_id: int
    created: datetime


class TrailRepository:
    def get_one(self, trail_id: int) -> Union[TrailOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *,
                        TO_CHAR(start_date, 'MM/DD/YY') AS start_date,
                        TO_CHAR(end_date, 'MM/DD/YY') AS end_date
                        FROM trail
                        WHERE id = %s
                        """,
                        [trail_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Trail not found",
                        )
                    record = result.fetchone()
                    if record is not None:
                        return self.record_to_trail_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get that Trail"}

    # get all trails for the current user
    def get_all_current_user(
        self, user_id: int
    ) -> Union[List[TrailOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        *,
                        TO_CHAR(start_date, 'MM/DD/YY') AS start_date,
                        TO_CHAR(end_date, 'MM/DD/YY') AS end_date
                        FROM trail
                        WHERE user_id = %s
                        ORDER BY created DESC;
                        """,
                        [user_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Trails not found",
                        )
                    result = db.fetchall()
                    return [
                        self.record_to_trail_out(record) for record in result
                    ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all of your trails"}

    def get_all(self) -> Union[List[TrailOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *,
                        TO_CHAR(start_date, 'MM/DD/YY') AS start_date,
                        TO_CHAR(end_date, 'MM/DD/YY') AS end_date
                        FROM trail
                        ORDER BY created DESC;
                        """
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Trails not found",
                        )
                    result = db.fetchall()
                    return [
                        self.record_to_trail_out(record) for record in result
                    ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all trails"}

    def create(self, user_id: int, trail: TrailIn) -> Union[TrailOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO trail
                        (
                            user_id,
                            name,
                            image_url,
                            location,
                            length,
                            lowest_elevation,
                            highest_elevation,
                            elevation_gain,
                            days,
                            nights,
                            start_date,
                            end_date,
                            loadout_id
                        )
                        VALUES
                        (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                        RETURNING *,
                        TO_CHAR(start_date, 'MM/DD/YY') AS start_date,
                        TO_CHAR(end_date, 'MM/DD/YY') AS end_date;
                        """,
                        [
                            user_id,
                            trail.name,
                            trail.image_url,
                            trail.location,
                            trail.length,
                            trail.lowest_elevation,
                            trail.highest_elevation,
                            trail.elevation_gain,
                            trail.days,
                            trail.nights,
                            trail.start_date,
                            trail.end_date,
                            trail.loadout_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Trail not created",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_trail_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not create trail"}

    def delete(self, trail_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM trail
                        WHERE id = %s
                        """,
                        [trail_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Trail not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    def update(self, trail_id: int, trail: TrailIn) -> Union[TrailOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE trail
                        SET name = %s
                        , image_url = %s
                        , location = %s
                        , length = %s
                        , lowest_elevation = %s
                        , highest_elevation = %s
                        , elevation_gain = %s
                        , days = %s
                        , nights = %s
                        , start_date = %s
                        , end_date = %s
                        , loadout_id = %s
                        WHERE id = %s
                        RETURNING *,
                        TO_CHAR(start_date, 'MM/DD/YY') AS start_date,
                        TO_CHAR(end_date, 'MM/DD/YY') AS end_date;
                        """,
                        [
                            trail.name,
                            trail.image_url,
                            trail.location,
                            trail.length,
                            trail.lowest_elevation,
                            trail.highest_elevation,
                            trail.elevation_gain,
                            trail.days,
                            trail.nights,
                            trail.start_date,
                            trail.end_date,
                            trail.loadout_id,
                            trail_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Trail not updated",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_trail_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not update category"}

    def record_to_trail_out(self, record):
        return TrailOut(
            id=record[0],
            user_id=record[1],
            name=record[2],
            image_url=record[3],
            location=record[4],
            length=record[5],
            lowest_elevation=record[6],
            highest_elevation=record[7],
            elevation_gain=record[8],
            days=record[9],
            nights=record[10],
            start_date=record[15],
            end_date=record[16],
            loadout_id=record[13],
            created=record[14],
        )
