from pydantic import BaseModel
from typing import List, Union
from datetime import datetime
from psycopg_pool import ConnectionPool
from fastapi import HTTPException, status
import os

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


# the person who follows someone is the user_id
# the person being followed is the following_id


class Error(BaseModel):
    message: str


class ConnectionIn(BaseModel):
    following_id: int  # the person who the user_id is following


class ConnectionOut(BaseModel):
    id: int
    user_id: int
    following_id: int
    created: datetime


class ConnectionRepository:
    def get_connection(
        self, connection_id: int
    ) -> Union[ConnectionOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM connections
                        WHERE id = %s
                        """,
                        [connection_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Connection not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_connection_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get connection"}

    # get connections for logged in user
    def get_connections_user(
        self, user_id: int
    ) -> Union[List[ConnectionOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM connections
                        WHERE user_id = %s
                        """,
                        [user_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Connections not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_connection_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all your connections"}

    def get_connections(self) -> Union[List[ConnectionOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM connections
                        """
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Connections not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_connection_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all connections"}

    def create_connection(
        self, user_id: int, connection: ConnectionIn
    ) -> Union[ConnectionOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO connections
                        (user_id, following_id)
                        VALUES
                        (%s, %s)
                        RETURNING *;
                        """,
                        [
                            user_id,
                            connection.following_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Connection not created",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_connection_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not create Connection"}

    def update_connection(
        self, connection_id: int, connection: ConnectionIn
    ) -> Union[ConnectionOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE connections
                        SET following_id = %s,
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [
                            connection.following_id,
                            connection_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Connection not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_connection_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not update a Connection with that ID"}

    def delete_connection(self, connection_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM connections
                        WHERE id = %s
                        """,
                        [connection_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Connection not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    def record_to_connection_out(self, record):
        return ConnectionOut(
            id=record[0],
            user_id=record[1],
            following_id=record[2],
            created=record[3],
        )
