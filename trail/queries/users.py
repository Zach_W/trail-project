from typing import List, Optional, Union
from pydantic import BaseModel
from psycopg_pool import ConnectionPool
from fastapi import HTTPException, status
import os

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


# IN MODEL


class Error(BaseModel):
    message: str


class UserLoginIn(BaseModel):
    email: str
    username: str
    password: str
    first_name: str
    last_name: str
    profile_picture: Optional[str]
    about: Optional[str]


class UserIn(BaseModel):
    email: str
    password: str
    first_name: str
    last_name: str
    profile_picture: Optional[str]
    about: Optional[str]


class UserOut(BaseModel):
    id: int
    email: str
    username: str
    first_name: str
    last_name: str
    profile_picture: Optional[str]
    about: Optional[str]


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserRepository:
    def get_user_with_password(
        self, username: str
    ) -> Union[UserOutWithPassword, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="User not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_user_out_with_password(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not find that user"}

    def get_one(self, user_id: int) -> Union[UserOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM users
                        WHERE id = %s
                        """,
                        [user_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="User not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_user_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not find that user"}

    def get_users(self) -> Union[List[UserOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM users
                        """
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="User not found",
                        )
                    result = db.fetchall()
                    return [
                        self.record_to_user_out(record) for record in result
                    ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all users"}

    def create_user(
        self, user: UserLoginIn, hashed_password: str
    ) -> Union[UserOutWithPassword, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO users(
                            email,
                            username,
                            password,
                            first_name,
                            last_name,
                            profile_picture,
                            about
                        )
                        VALUES ( %s, %s, %s, %s, %s, %s,%s)
                        RETURNING *;
                        """,
                        [
                            user.email,
                            user.username,
                            hashed_password,
                            user.first_name,
                            user.last_name,
                            user.profile_picture,
                            user.about,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="User not created",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_user_out_with_password(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {
                "message": "Could not create user, check the data you inputted"
            }

    def delete_user(self, username: str) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM users
                        where username = %s
                        """,
                        [username],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="User not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    def update_user(
        self, username: str, user: UserIn
    ) -> Union[UserOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE users
                        SET first_name = %s
                        , last_name = %s
                        , profile_picture = %s
                        , about = %s
                        WHERE username = %s
                        RETURNING *;
                        """,
                        [
                            user.first_name,
                            user.last_name,
                            user.profile_picture,
                            user.about,
                            username,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Could not update user",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_user_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not update that user"}

    def record_to_user_out(self, record):
        return UserOut(
            id=record[0],
            email=record[1],
            username=record[2],
            first_name=record[4],
            last_name=record[5],
            profile_picture=record[6],
            about=record[7],
        )

    def record_to_user_out_with_password(self, record):
        return UserOutWithPassword(
            id=record[0],
            email=record[1],
            username=record[2],
            hashed_password=record[3],
            first_name=record[4],
            last_name=record[5],
            profile_picture=record[6],
            about=record[7],
        )
