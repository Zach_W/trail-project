from typing import List, Union
from pydantic import BaseModel
from queries.pool import pool
from fastapi import HTTPException, status
from decimal import Decimal


class Error(BaseModel):
    message: str


class LoadoutIn(BaseModel):
    name: str
    season: str
    total_weight: Decimal


class LoadoutOut(BaseModel):
    id: int
    user_id: int
    name: str
    season: str
    total_weight: Decimal


class LoadoutRepository:
    # get loadout info
    def get_one(self, loadout_id: int) -> Union[LoadoutOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM loadout
                        WHERE id = %s
                        """,
                        [loadout_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Loadout not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_loadout_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get that Loadout"}

    # get all loadouts for a user
    def get_all(self, user_id: int) -> Union[List[LoadoutOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM loadout
                        WHERE user_id = %s
                        ORDER BY id;
                        """,
                        [user_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Loadouts not found",
                        )
                    result = db.fetchall()
                    return [
                        self.record_to_loadout_out(record) for record in result
                    ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all of your loadouts"}

    # Create loadout
    def create(
        self, user_id: int, loadout: LoadoutIn
    ) -> Union[LoadoutOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO loadout
                        (user_id, name, season, total_weight)
                        VALUES
                        (%s,%s,%s,%s)
                        RETURNING *;
                        """,
                        [
                            user_id,
                            loadout.name,
                            loadout.season,
                            loadout.total_weight,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Loadout not created",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_loadout_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not create loadout"}

    # delete loadout
    def delete(self, loadout_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM loadout
                        WHERE id = %s
                        """,
                        [loadout_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="loadout not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    # update loadout
    def update(
        self, loadout_id: int, loadout: LoadoutIn
    ) -> Union[LoadoutOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE loadout
                        SET name = %s
                        , season = %s
                        , total_weight = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [
                            loadout.name,
                            loadout.season,
                            loadout.total_weight,
                            loadout_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Loadout not updated",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_loadout_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not update loadout"}

    def record_to_loadout_out(self, record):
        return LoadoutOut(
            id=record[0],
            user_id=record[1],
            name=record[2],
            season=record[3],
            total_weight=record[4],
        )
