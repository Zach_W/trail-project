from pydantic import BaseModel
from typing import List, Union
from psycopg_pool import ConnectionPool
from fastapi import HTTPException, status
import os


pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class Error(BaseModel):
    message: str


class SocialsIn(BaseModel):
    name: str
    link: str


class SocialsOut(BaseModel):
    id: int
    user_id: int
    name: str
    link: str


class SocialsRepository:
    def get_social(self, social_id: int) -> Union[SocialsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM socials
                        WHERE id = %s
                        """,
                        [social_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Social not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_social_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"Message": "Could not find Social"}

    def get_user_socials(self, user_id: int) -> Union[List[SocialsOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM socials
                        WHERE user_id = %s
                        ORDER BY id;
                        """,
                        [user_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Socials not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_social_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all socials"}

    def get_socials(self) -> Union[List[SocialsOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM socials
                        ORDER BY id;
                        """
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Socials not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_social_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all socials"}

    def create_social(
        self, user_id: int, social: SocialsIn
    ) -> Union[SocialsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO socials
                        (user_id, name, link)
                        VALUES
                        (%s, %s, %s)
                        RETURNING *
                        """,
                        [
                            user_id,
                            social.name,
                            social.link,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Social not created",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_social_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not create Social"}

    def delete_social(self, social_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM socials
                        where id = %s
                        """,
                        [social_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Social not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    def update_social(
        self, social_id: int, social: SocialsIn
    ) -> Union[SocialsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE socials
                        SET name = %s
                        , link = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [
                            social.name,
                            social.link,
                            social_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Social not updated",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_social_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not update that social"}

    def record_to_social_out(self, record):
        return SocialsOut(
            id=record[0],
            user_id=record[1],
            name=record[2],
            link=record[3],
        )
