from typing import List, Union
from pydantic import BaseModel
from queries.pool import pool
from fastapi import HTTPException, status


# Error
class Error(BaseModel):
    message: str


# Like in
class LikesIn(BaseModel):
    trail_id: int


# Like Out
class LikesOut(BaseModel):
    id: int
    trail_id: int
    username: str


# Repo Class
class LikesRepository:
    # get one
    def get_one(self, like_id: int) -> Union[LikesOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM likes
                        WHERE id = %s
                        """,
                        [like_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Like not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_like_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get that Like"}

    # get all for a post
    def get_all(self, trail_id: int) -> Union[List[LikesOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM likes
                        WHERE trail_id = %s
                        ORDER BY username;
                        """,
                        [trail_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Likes not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_like_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all Likes"}

    # create
    def create(self, username: str, like: LikesIn) -> Union[LikesOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO likes
                        (trail_id,username)
                        VALUES
                        (%s,%s)
                        RETURNING *;
                        """,
                        [like.trail_id, username],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Like not created",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_like_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Creating like did not work"}

    # delete
    def delete(self, like_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM likes
                        WHERE id = %s
                        """,
                        [like_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Like not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    # record to like out
    def record_to_like_out(self, record):
        return LikesOut(
            id=record[0],
            trail_id=record[1],
            username=record[2],
        )
