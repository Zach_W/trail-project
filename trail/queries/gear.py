from typing import List, Union
from pydantic import BaseModel
from queries.pool import pool
from fastapi import HTTPException, status
from decimal import Decimal


# Error
class Error(BaseModel):
    message: str


class GearIn(BaseModel):
    name: str
    quantity: int
    loadout_id: int
    category_id: int


class GearOut(BaseModel):
    id: int
    user_id: int
    name: str
    quantity: int
    loadout_id: int
    category_id: int


class GearRepository:
    def get_one(self, gear_id: int) -> Union[GearOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM gear
                        WHERE id = %s
                        """,
                        [gear_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Gear not found",
                        )
                    record = result.fetchone()
                    if record is not None:
                        return self.record_to_gear_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get that Gear"}

    # get all gear for current Loadout
    def get_all_for_loadout(
        self, loadout_id: int
    ) -> Union[List[GearOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM gear
                        WHERE loadout_id = %s
                        ORDER BY id;
                        """,
                        [loadout_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Gears not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_gear_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all of your gear"}

    # get all gear for current user
    def get_all(self, user_id: int) -> Union[List[GearOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM gear
                        WHERE user_id = %s
                        ORDER BY id;
                        """,
                        [user_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Gears not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_gear_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all of your gear"}

    def create(self, user_id: int, gear: GearIn) -> Union[GearOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO gear
                        (
                            user_id,
                            name,
                            quantity,
                            loadout_id,
                            category_id
                        )
                        VALUES
                        (%s,%s,%s,%s,%s)
                        RETURNING *;
                        """,
                        [
                            user_id,
                            gear.name,
                            gear.quantity,
                            gear.loadout_id,
                            gear.category_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Gear not created",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_gear_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not create gear"}

    def delete(self, gear_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM gear
                        WHERE id = %s
                        """,
                        [gear_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Gear not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    def update(self, gear_id: int, gear: GearIn) -> Union[GearOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE gear
                        SET name = %s
                        , quantity = %s
                        , loadout_id = %s
                        , category_id = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [
                            gear.name,
                            gear.quantity,
                            gear.loadout_id,
                            gear.category_id,
                            gear_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Gear not updated",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_gear_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not update gear"}

    def record_to_gear_out(self, record):
        return GearOut(
            id=record[0],
            user_id=record[1],
            name=record[2],
            quantity=record[5],
            loadout_id=record[6],
            category_id=record[7],
        )
