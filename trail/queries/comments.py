from typing import List, Union
from datetime import datetime
from pydantic import BaseModel
from queries.pool import pool
from fastapi import HTTPException, status


# Error
class Error(BaseModel):
    message: str


# Comment in
class CommentsIn(BaseModel):
    trail_id: int
    text: str


# Comment Out
class CommentsOut(BaseModel):
    id: int
    trail_id: int
    username: str
    text: str
    created: datetime


# Repo Class
class CommentsRepository:
    # get one
    def get_one(self, comment_id: int) -> Union[CommentsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
            SELECT *
            FROM comments
            WHERE id = %s
            """,
                        [comment_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Comment not found",
                        )
                    record = result.fetchone()
                    if record is not None:
                        return self.record_to_comment_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get that Comment"}

    # get all
    def get_all(self) -> Union[List[CommentsOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM comments
                        ORDER BY created;
                        """
                    )
                    if result is not None:
                        return [
                            self.record_to_comment_out(record)
                            for record in result
                        ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all Comments"}

    # get all for one post
    def get_all_for_post(
        self, trail_id: int
    ) -> Union[List[CommentsOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM comments
                        WHERE trail_id = %s
                        ORDER BY created;
                        """,
                        [trail_id],
                    )
                    if result is not None:
                        return [
                            self.record_to_comment_out(record)
                            for record in result
                        ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all Comments"}

    # create
    def create(
        self, username: str, comment: CommentsIn
    ) -> Union[CommentsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO comments
                          (trail_id,username,text)
                        VALUES
                          (%s,%s,%s)
                        RETURNING *;
                        """,
                        [
                            comment.trail_id,
                            username,
                            comment.text,
                        ],
                    )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_comment_out(record)
        except Exception as e:
            print(e)
            return {"message": "Creating comment did not work"}

    # delete
    def delete(self, comment_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM comments
                        WHERE id = %s
                        """,
                        [comment_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Comment not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    # update
    def update(
        self, comment_id: int, comment: CommentsIn
    ) -> Union[CommentsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE comments
                        SET text = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [
                            comment.text,
                            comment_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Comment not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_comment_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not update Comment"}

    # record to comment out
    def record_to_comment_out(self, record):
        return CommentsOut(
            id=record[0],
            trail_id=record[1],
            username=record[2],
            text=record[3],
            created=record[4],
        )
