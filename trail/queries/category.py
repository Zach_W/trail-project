from typing import List, Union
from pydantic import BaseModel
from queries.pool import pool
from fastapi import HTTPException, status


class Error(BaseModel):
    message: str


class CategoryIn(BaseModel):
    name: str
    loadout_id: int


class CategoryOut(BaseModel):
    id: int
    user_id: int
    name: str
    loadout_id: int


class CategoryRepository:
    def get_one(self, category_id: int) -> Union[CategoryOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM category
                        WHERE id = %s
                        """,
                        [category_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Category not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_category_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get that Category"}

    # get all categories for a specific user
    def get_all_for_loadout(
        self, loadout_id: int
    ) -> Union[List[CategoryOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM category
                        WHERE loadout_id = %s
                        ORDER BY id;
                        """,
                        [loadout_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Categories not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_category_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all of your categories"}

    # get all categories for a specific user
    def get_all_user_id(self, user_id: int) -> Union[List[CategoryOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM category
                        WHERE user_id = %s
                        ORDER BY id;
                        """,
                        [user_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Categories not found",
                        )
                    result = db.fetchall()
                    if result is not None:
                        return [
                            self.record_to_category_out(record)
                            for record in result
                        ]
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not get all of your categories"}

    def create(
        self, user_id: int, category: CategoryIn
    ) -> Union[CategoryOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO category
                        (user_id, name, loadout_id)
                        VALUES
                        (%s,%s,%s)
                        RETURNING *;
                        """,
                        [
                            user_id,
                            category.name,
                            category.loadout_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Category not created",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_category_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not create category"}

    def delete(self, category_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM category
                        WHERE id = %s
                        """,
                        [category_id],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Category not deleted",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False

    def update(
        self, category_id: int, category: CategoryIn
    ) -> Union[CategoryOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE category
                        SET name = %s
                        , loadout_id = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [
                            category.name,
                            category.loadout_id,
                            category_id,
                        ],
                    )
                    if db.rowcount <= 0:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Category not found",
                        )
                    record = db.fetchone()
                    if record is not None:
                        return self.record_to_category_out(record)
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return {"message": "Could not update category"}

    def record_to_category_out(self, record):
        return CategoryOut(
            id=record[0],
            user_id=record[1],
            name=record[2],
            loadout_id=record[3],
        )
