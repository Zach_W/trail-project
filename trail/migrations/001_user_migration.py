steps = [
    [
        """
        CREATE TABLE users (
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            email VARCHAR(100) NOT NULL UNIQUE,
            username VARCHAR(20) UNIQUE NOT NULL,
            password VARCHAR(128) NOT NULL,
            first_name  VARCHAR(50) NOT NULL,
            last_name  VARCHAR(50) NOT NULL,
            profile_picture VARCHAR(2500),
            about VARCHAR(1250)
        );
        """,
        """
        DROP TABLE users;
        """,
    ],
    [
        """
        CREATE TABLE connections(
            id SERIAL PRIMARY KEY,
            user_id INT NOT NULL,
            following_id INT NOT NULL,
            created TIMESTAMP DEFAULT NOW(),
            UNIQUE (user_id , following_id ),
            CHECK (user_id <> following_id ),
            FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
            FOREIGN KEY (following_id) REFERENCES users(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE connections;
        """,
    ],
    [
        """
        CREATE TABLE socials(
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            user_id INT NOT NULL,
            name VARCHAR(50) NOT NULL,
            link VARCHAR(2500) NOT NULL,
            UNIQUE (user_id , name),
            FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE socials;
        """,
    ],
    [
        """
        CREATE TABLE loadout(
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            user_id INT NOT NULL,
            name VARCHAR(30) NOT NULL,
            season VARCHAR(30) NOT NULL,
            total_weight DECIMAL(12,2),
            UNIQUE (user_id, name),
            FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE loadout;
        """,
    ],
    [
        """
        CREATE TABLE trail(
        id SERIAL NOT NULL UNIQUE PRIMARY KEY,
        user_id INT NOT NULL,
        name VARCHAR(150) NOT NULL,
        image_url VARCHAR(2500),
        location VARCHAR(150) NOT NULL,
        length DECIMAL(12,2) NOT NULL,
        lowest_elevation INT,
        highest_elevation INT,
        elevation_gain INT,
        days INT NOT NULL,
        nights INT NOT NULL,
        start_date DATE NOT NULL,
        end_date DATE NOT NULL,
        loadout_id INT NOT NULL,
        created TIMESTAMP DEFAULT NOW(),
        UNIQUE (user_id, name),
        FOREIGN KEY (loadout_id) REFERENCES loadout(id) ON DELETE SET NULL,
        FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE trail;
        """,
    ],
    [
        """
        CREATE TABLE comments(
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            trail_id INT NOT NULL,
            username VARCHAR(20) NOT NULL,
            text VARCHAR(600) NOT NULL,
            created TIMESTAMP DEFAULT NOW(),
            FOREIGN KEY (trail_id) REFERENCES trail(id) ON DELETE CASCADE,
            FOREIGN KEY (username) REFERENCES users(username) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE comments;
        """,
    ],
    [
        """
        CREATE TABLE likes(
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            trail_id INT NOT NULL,
            username VARCHAR(20) NOT NULL,
            UNIQUE (username , trail_id ),
            FOREIGN KEY (trail_id) REFERENCES trail(id) ON DELETE CASCADE,
            FOREIGN KEY (username) REFERENCES users(username) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE likes;
        """,
    ],
    [
        """
        CREATE TABLE category(
            id SERIAL NOT NULL UNIQUE PRIMARY KEY,
            user_id INT NOT NULL,
            name VARCHAR(30) NOT NULL,
            loadout_id INT NOT NULL,
            UNIQUE (user_id , name),
            FOREIGN KEY (loadout_id) REFERENCES loadout(id) ON DELETE CASCADE,
            FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE category;
        """,
    ],
    [
        """
        CREATE TABLE gear(
        id SERIAL NOT NULL UNIQUE PRIMARY KEY,
        user_id INT NOT NULL,
        name VARCHAR(30) NOT NULL,
        quantity INT NOT NULL DEFAULT 1,
        loadout_id INT NOT NULL,
        category_id INT NOT NULL,
        UNIQUE (user_id , name),
        FOREIGN KEY (loadout_id) REFERENCES loadout(id) ON DELETE CASCADE,
        FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE CASCADE,
        FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE gear;
        """,
    ],
]
