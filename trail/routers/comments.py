from typing import List, Union
from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from queries.comments import (
    Error,
    CommentsIn,
    CommentsRepository,
    CommentsOut,
)

router = APIRouter(prefix="/api")


@router.get("/comments/{comment_id}", response_model=Union[CommentsOut, Error])
def get_one_comment(
    comment_id: int,
    response: Response,
    repo: CommentsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_one(comment_id)


@router.get("/comments/", response_model=Union[List[CommentsOut], Error])
def get_all_comments(
    repo: CommentsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all()


@router.get(
    "/comments/trail/{trail_id}",
    response_model=Union[List[CommentsOut], Error],
)
def get_all_comments_for_post(
    trail_id: int,
    repo: CommentsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all_for_post(trail_id)


@router.post("/comments/", response_model=Union[CommentsOut, Error])
def create_comment(
    comment: CommentsIn,
    response: Response,
    repo: CommentsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(user_data.get("username"), comment)


@router.delete("/comments/{comment_id}", response_model=Union[bool, Error])
def delete_comment(
    comment_id: int,
    response: Response,
    repo: CommentsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(comment_id)


@router.put("/comments/{comment_id}", response_model=Union[CommentsOut, Error])
def update_comment(
    comment_id: int,
    comment: CommentsIn,
    response: Response,
    repo: CommentsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update(comment_id, comment)
