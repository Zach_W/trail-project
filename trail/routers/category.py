from typing import List, Union
from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from queries.category import (
    Error,
    CategoryIn,
    CategoryRepository,
    CategoryOut,
)

router = APIRouter(prefix="/api")


@router.get(
    "/categories/{category_id}", response_model=Union[CategoryOut, Error]
)
def get_one_category(
    category_id: int,
    response: Response,
    repo: CategoryRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    category = repo.get_one(category_id)
    if category is None:
        response.status_code = 404
    return category


# get all categories for the logged in user
@router.get(
    "/categories/currentUser/", response_model=Union[List[CategoryOut], Error]
)
def get_all_current_user_categories(
    repo: CategoryRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all_user_id(user_data.get("id"))


@router.get(
    "/categories/loadout/{loadout_id}",
    response_model=Union[List[CategoryOut], Error],
)
def get_all_categories_for_loadout(
    loadout_id: int,
    repo: CategoryRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all_for_loadout(loadout_id)


# get all categories for a specific user
@router.get(
    "/categories/user/{user_id}",
    response_model=Union[List[CategoryOut], Error],
)
def get_all_user_categories(
    user_id: int,
    repo: CategoryRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all_user_id(user_id)


@router.post("/categories/", response_model=Union[CategoryOut, Error])
def create_category(
    category: CategoryIn,
    response: Response,
    repo: CategoryRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(user_data.get("id"), category)


@router.delete("/categories/{category_id}", response_model=Union[bool, Error])
def delete_category(
    category_id: int,
    response: Response,
    repo: CategoryRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(category_id)


@router.put(
    "/categories/{category_id}", response_model=Union[CategoryOut, Error]
)
def update_category(
    category_id: int,
    category: CategoryIn,
    response: Response,
    repo: CategoryRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update(category_id, category)
