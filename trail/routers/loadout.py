from typing import List, Union
from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from queries.loadout import (
    Error,
    LoadoutIn,
    LoadoutRepository,
    LoadoutOut,
)

router = APIRouter(prefix="/api")


@router.post("/loadouts/", response_model=Union[LoadoutOut, Error])
def create_loadout(
    loadout: LoadoutIn,
    response: Response,
    repo: LoadoutRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(user_data.get("id"), loadout)


# get all loadouts for the logged in user
@router.get(
    "/loadouts/currentUser/", response_model=Union[List[LoadoutOut], Error]
)
def get_all_current_user_loadouts(
    repo: LoadoutRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all(user_data.get("id"))


# get all loadouts for a specified user
@router.get(
    "/loadouts/user/{user_id}", response_model=Union[List[LoadoutOut], Error]
)
def get_all_user_loadouts(
    user_id: int,
    repo: LoadoutRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all(user_id)


@router.put("/loadouts/{loadout_id}", response_model=Union[LoadoutOut, Error])
def update_loadout(
    loadout_id: int,
    loadout: LoadoutIn,
    response: Response,
    repo: LoadoutRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update(loadout_id, loadout)


@router.delete("/loadouts/{loadout_id}", response_model=Union[bool, Error])
def delete_loadout(
    loadout_id: int,
    response: Response,
    repo: LoadoutRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(loadout_id)


@router.get("/loadouts/{loadout_id}", response_model=Union[LoadoutOut, Error])
def get_one_loadout(
    loadout_id: int,
    response: Response,
    repo: LoadoutRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_one(loadout_id)
