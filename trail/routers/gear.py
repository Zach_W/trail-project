from typing import List, Union
from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from queries.gear import (
    Error,
    GearIn,
    GearRepository,
    GearOut,
)

router = APIRouter(prefix="/api")


@router.post("/gears/", response_model=Union[GearOut, Error])
def create_gear(
    gear: GearIn,
    response: Response,
    repo: GearRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(user_data.get("id"), gear)


@router.get("/gears/currentUser/", response_model=Union[List[GearOut], Error])
def get_all_current_user_gears(
    repo: GearRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all(user_data.get("id"))


@router.get(
    "/gears/loadout/{loadout_id}", response_model=Union[List[GearOut], Error]
)
def get_all_gears_for_loadout(
    loadout_id: int,
    repo: GearRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all_for_loadout(loadout_id)


@router.put("/gears/{gear_id}", response_model=Union[GearOut, Error])
def update_gear(
    gear_id: int,
    gear: GearIn,
    response: Response,
    repo: GearRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update(gear_id, gear)


@router.delete("/gears/{gear_id}", response_model=Union[bool, Error])
def delete_gear(
    gear_id: int,
    response: Response,
    repo: GearRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(gear_id)


@router.get("/gears/{gear_id}", response_model=Union[GearOut, Error])
def get_one_gear(
    gear_id: int,
    response: Response,
    repo: GearRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_one(gear_id)
