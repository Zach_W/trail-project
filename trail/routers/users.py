from typing import List, Union, Optional
from fastapi import (
    Depends,
    APIRouter,
    HTTPException,
    status,
    Response,
    Request,
)
from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from fastapi.responses import JSONResponse
from queries.users import (
    UserIn,
    UserLoginIn,
    UserOut,
    UserRepository,
    Error,
)


class UserForm(BaseModel):
    username: str
    password: str


class UserToken(Token):
    user: UserOut


router = APIRouter()


@router.get("/token", response_model=UserToken | None)
async def get_token(
    request: Request,
    user: Union[UserOut, None] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if user is None:
        return None
    if user is not None:
        if authenticator.cookie_name in request.cookies:
            return {
                "access_token": request.cookies[authenticator.cookie_name],
                "type": "Bearer",
                "user": user,
            }


@router.get("/checkToken")
async def check_token(
    request: Request,
    user: Optional[dict] = Depends(authenticator.try_get_current_account_data),
):
    if user is not None:
        return JSONResponse(content=True, status_code=200)
    else:
        return JSONResponse(content=False, status_code=201)


@router.get("/api/users/{user_id}", response_model=Union[UserOut, Error])
async def get_one_user(
    user_id: int,
    response: Response,
    repo: UserRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_one(user_id)


@router.get("/api/users/currentUser/", response_model=Union[UserOut, Error])
async def get_logged_in_user(
    response: Response,
    repo: UserRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_one(user_data.get("id"))


@router.get("/api/users/", response_model=Union[List[UserOut], Error])
async def get_users(
    repo: UserRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_users()


@router.post("/api/users/", response_model=Union[UserToken, Error])
async def create_user(
    user_info: UserLoginIn,
    request: Request,
    response: Response,
    repo: UserRepository = Depends(),
):
    hashed_password = authenticator.hash_password(user_info.password)
    try:
        user = repo.create_user(user_info, hashed_password)
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = UserForm(username=user_info.email, password=user_info.password)
    token = await authenticator.login(response, request, form, repo)
    return UserToken(user=user, **token.dict())


@router.delete("/api/users/{username}", response_model=Union[bool, Error])
def delete_user(
    username: str,
    response: Response,
    repo: UserRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete_user(username)


@router.put("/api/users/{username}", response_model=Union[UserOut, Error])
async def update_user(
    user: UserIn,
    repo: UserRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update_user(user_data.get("username"), user)
