from fastapi import APIRouter, Depends, Response
from typing import List, Union
from authenticator import authenticator
from queries.connections import (
    ConnectionIn,
    ConnectionOut,
    ConnectionRepository,
    Error,
)

router = APIRouter(prefix="/api")


@router.get(
    "/connections/{connection_id}", response_model=Union[ConnectionOut, Error]
)
def get_one_connection(
    connection_id: int,
    response: Response,
    repo: ConnectionRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_connection(connection_id)


@router.get(
    "/connections/currentUser/",
    response_model=Union[List[ConnectionOut], Error],
)
def get_all_current_user_connections(
    repo: ConnectionRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_connections_user(user_data.get("id"))


@router.get(
    "/connections/user/{user_id}",
    response_model=Union[List[ConnectionOut], Error],
)
def get_all_user_connections(
    user_id: int,
    repo: ConnectionRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_connections_user(user_id)


@router.get("/connections/", response_model=Union[List[ConnectionOut], Error])
def get_connections(
    repo: ConnectionRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_connections()


@router.post("/connections/", response_model=Union[ConnectionOut, Error])
def create_connection(
    connection: ConnectionIn,
    response: Response,
    repo: ConnectionRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create_connection(user_data.get("id"), connection)


@router.delete(
    "/connections/{connection_id}", response_model=Union[bool, Error]
)
def delete_connection(
    connection_id: int,
    response: Response,
    repo: ConnectionRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete_connection(connection_id)


@router.put(
    "/connections/{connection_id}", response_model=Union[ConnectionOut, Error]
)
def update_connection(
    connection_id: int,
    connection: ConnectionIn,
    response: Response,
    repo: ConnectionRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update_connection(connection_id, connection)
