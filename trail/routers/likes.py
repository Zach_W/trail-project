from typing import List, Union
from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from queries.likes import (
    Error,
    LikesIn,
    LikesRepository,
    LikesOut,
)

router = APIRouter(prefix="/api")


@router.post("/likes/", response_model=Union[LikesOut, Error])
def create_like(
    like: LikesIn,
    response: Response,
    repo: LikesRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(user_data.get("username"), like)


@router.get(
    "/likes/trail/{trail_id}", response_model=Union[List[LikesOut], Error]
)
def get_all_likes_for_a_trail(
    trail_id: int,
    repo: LikesRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all(trail_id)


@router.delete("/likes/{like_id}", response_model=Union[bool, Error])
def delete_like(
    like_id: int,
    response: Response,
    repo: LikesRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(like_id)


@router.get("/likes/{like_id}", response_model=Union[LikesOut, Error])
def get_one_like(
    like_id: int,
    response: Response,
    repo: LikesRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_one(like_id)
