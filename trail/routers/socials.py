from fastapi import APIRouter, Depends, Response
from typing import List, Union
from authenticator import authenticator
from queries.socials import SocialsIn, SocialsOut, SocialsRepository, Error

router = APIRouter(prefix="/api")


@router.post("/socials/", response_model=Union[SocialsOut, Error])
def create_social(
    social: SocialsIn,
    response: Response,
    repo: SocialsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create_social(user_data.get("id"), social)


@router.get("/socials/", response_model=Union[List[SocialsOut], Error])
def get_socials(
    repo: SocialsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_socials()


@router.get(
    "/socials/currentUser/", response_model=Union[List[SocialsOut], Error]
)
def get_current_users_socials(
    repo: SocialsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_user_socials(user_data.get("id"))


@router.get(
    "/socials/user/{user_id}", response_model=Union[List[SocialsOut], Error]
)
def get_users_socials(
    user_id: int,
    repo: SocialsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_user_socials(user_id)


@router.get("/socials/{social_id}", response_model=Union[SocialsOut, Error])
def get_one_social(
    social_id: int,
    response: Response,
    repo: SocialsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_social(social_id)


@router.put("/socials/{social_id}", response_model=Union[SocialsOut, Error])
def update_social(
    social_id: int,
    social: SocialsIn,
    response: Response,
    repo: SocialsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update_social(social_id, social)


@router.delete("/socials/{social_id}", response_model=Union[bool, Error])
def delete_social(
    social_id: int,
    response: Response,
    repo: SocialsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete_social(social_id)
