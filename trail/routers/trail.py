from typing import List, Union, Optional
from pydantic import BaseModel
from fastapi import APIRouter, Depends, Response, HTTPException
from authenticator import authenticator
from queries.trail import Error, TrailIn, TrailOut, TrailRepository
from queries.loadout import LoadoutOut, LoadoutRepository
from queries.gear import GearOut, GearRepository
from queries.users import UserOut, UserRepository
from queries.category import CategoryOut, CategoryRepository

router = APIRouter(prefix="/api")


class TrailFeed(BaseModel):
    trail: TrailOut
    loadout: LoadoutOut
    user: UserOut


class TrailDetails(BaseModel):
    trail: Optional[TrailOut]
    loadout: Optional[LoadoutOut]
    user: Optional[UserOut]
    gear: Optional[List[GearOut]]
    categories: Optional[List[CategoryOut]]


@router.get("/trails/{trail_id}", response_model=Union[TrailDetails, Error])
def get_trail_details(
    trail_id: int,
    response: Response,
    trail_repo: TrailRepository = Depends(),
    loadout_repo: LoadoutRepository = Depends(),
    user_repo: UserRepository = Depends(),
    gear_repo: GearRepository = Depends(),
    category_repo: CategoryRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    trail = trail_repo.get_one(trail_id)
    if trail is None:
        raise HTTPException(status_code=404, detail="Trail not found")

    try:
        loadout = loadout_repo.get_one(trail.loadout_id)
    except Exception:
        loadout = None
    try:
        user = user_repo.get_one(trail.user_id)
    except Exception:
        user = None
    try:
        gear = gear_repo.get_all_for_loadout(trail.loadout_id)
    except Exception:
        gear = None
    try:
        categories = category_repo.get_all_for_loadout(trail.loadout_id)
    except Exception:
        categories = None

    trail_details = TrailDetails(
        trail=trail,
        loadout=loadout,
        user=user,
        gear=gear,
        categories=categories,
    )

    return trail_details


@router.get(
    "/trails/currentUser/", response_model=Union[List[TrailOut], Error]
)
async def get_all_current_users_trails(
    repo: TrailRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all_current_user(user_data.get("id"))


@router.get("/trails/", response_model=Union[List[TrailFeed], Error])
async def get_all_trails_feed(
    repo: TrailRepository = Depends(),
    loadout_repo: LoadoutRepository = Depends(),
    user_repo: UserRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    trails_with_loadout = []
    trails = repo.get_all()
    trails_with_loadout = [
        {
            "trail": trail,
            "loadout": loadout_repo.get_one(trail.loadout_id),
            "user": user_repo.get_one(trail.user_id),
        }
        for trail in trails
    ]
    return trails_with_loadout


@router.post("/trails/", response_model=Union[TrailOut, Error])
def create_trail(
    trail: TrailIn,
    response: Response,
    repo: TrailRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(user_data.get("id"), trail)


@router.delete("/trails/{trail_id}", response_model=Union[bool, Error])
def delete_trail(
    trail_id: int,
    response: Response,
    repo: TrailRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(trail_id)


@router.put("/trails/{trail_id}", response_model=Union[TrailOut, Error])
def update_trail(
    trail_id: int,
    trail: TrailIn,
    response: Response,
    repo: TrailRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update(trail_id, trail)
