from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from routers import (
    category,
    comments,
    connections,
    gear,
    likes,
    loadout,
    socials,
    trail,
    users,
)
import os

app = FastAPI()
app.include_router(authenticator.router)
app.include_router(category.router)
app.include_router(comments.router)
app.include_router(connections.router)
app.include_router(gear.router)
app.include_router(likes.router)
app.include_router(loadout.router)
app.include_router(socials.router)
app.include_router(trail.router)
app.include_router(users.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }
